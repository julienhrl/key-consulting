import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ville } from '../../../core/models/ville';
import { RequestService } from '../../../core/services/request.service';
import { Departement } from '../../../core/models/departement';

@Component({
  selector: 'app-france-ville',
  templateUrl: './france-ville.component.html',
  styleUrl: './france-ville.component.scss'
})
export class FranceVilleComponent {

  public villes : Ville[] = []
  public idDepartement !: number
  public currentDpt !: Departement

  constructor(
    private activeRoute : ActivatedRoute,
    private requestService : RequestService
  ){
    this.idDepartement = this.activeRoute.snapshot.params['idDepartement']
    this.requestService.getVillesByCodeDepartement(this.idDepartement.toString()).subscribe(villes => {
      this.villes = villes
    })

    this.requestService.getDepartementByCode(this.idDepartement.toString()).subscribe(departement => {
      this.currentDpt = departement[0]
      console.log(this.currentDpt)

    })
  }
}
