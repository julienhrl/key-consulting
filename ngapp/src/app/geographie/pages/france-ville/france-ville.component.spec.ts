import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FranceVilleComponent } from './france-ville.component';

describe('FranceVilleComponent', () => {
  let component: FranceVilleComponent;
  let fixture: ComponentFixture<FranceVilleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FranceVilleComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FranceVilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
