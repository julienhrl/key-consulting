import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FranceRegionComponent } from './france-region.component';

describe('FranceRegionComponent', () => {
  let component: FranceRegionComponent;
  let fixture: ComponentFixture<FranceRegionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FranceRegionComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FranceRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
