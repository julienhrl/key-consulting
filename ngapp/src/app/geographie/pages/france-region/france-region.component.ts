import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../../core/services/request.service';
import { Region } from '../../../core/models/region';
import { Departement } from '../../../core/models/departement';
import {Router} from "@angular/router"

@Component({
  selector: 'app-france-region',
  templateUrl: './france-region.component.html',
  styleUrl: './france-region.component.scss',
})
export class FranceRegionComponent implements OnInit{

  public regions !: Region[]

  constructor(
    private requestService : RequestService,
    private router : Router
  ){}

  ngOnInit(): void {
    this.requestService.getAllRegion().subscribe(regions =>{
      this.regions = regions
    })
  }  

  public redirectToVille(departement : Departement){
    this.router.navigate(['ville/'+departement.code])
  }
}
