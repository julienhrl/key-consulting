import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeographieRoutingModule } from './geographie-routing.module';
import { FranceRegionComponent } from './pages/france-region/france-region.component';
import { FormRegionComponent } from './components/form-region/form-region.component';
import { CoreModule } from '../core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FranceVilleComponent } from './pages/france-ville/france-ville.component';
import { TabVillesComponent } from './components/tab-villes/tab-villes.component';


@NgModule({
  declarations: [
    FranceRegionComponent,
    FormRegionComponent,
    FranceVilleComponent,
    TabVillesComponent
  ],
  imports: [
    CommonModule,
    GeographieRoutingModule,
    CoreModule,
  ]
})
export class GeographieModule { }
