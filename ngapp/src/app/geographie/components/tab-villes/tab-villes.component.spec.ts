import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabVillesComponent } from './tab-villes.component';

describe('TabVillesComponent', () => {
  let component: TabVillesComponent;
  let fixture: ComponentFixture<TabVillesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TabVillesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TabVillesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
