import { Component, Input, ViewChild } from '@angular/core';
import { Ville } from '../../../core/models/ville';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-tab-villes',
  templateUrl: './tab-villes.component.html',
  styleUrl: './tab-villes.component.scss'
})
export class TabVillesComponent {
  @Input() set villes (value : Ville[]){
    this.dataSource = new MatTableDataSource<Ville>(value)
  }

  displayedColumns: string[] = ['nom', 'cp', 'population'];
  dataSource !: MatTableDataSource<Ville>;

  @ViewChild(MatPaginator) paginator !: MatPaginator;
  @ViewChild(MatSort) sort !: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
