import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormControl} from '@angular/forms';
import { Region } from '../../../core/models/region';
import { RequestService } from '../../../core/services/request.service';
import { Departement } from '../../../core/models/departement';

@Component({
  selector: 'app-form-region',
  templateUrl: './form-region.component.html',
  styleUrl: './form-region.component.scss'
})
export class FormRegionComponent {
  
  @Input() set regions (value : Region[]){
    this.options = value
    this.filteredOptions = this.options.slice()
  }

  @Output() viewDepartementEvent : EventEmitter<Departement> = new EventEmitter<Departement>()

  @ViewChild('input') input !: ElementRef<HTMLInputElement>;
  region_FC = new FormControl<number | string>('');
  options: Region[] = [];
  filteredOptions !: Region[];

  public departements : Departement[] = []
  public displayedColumns: string[] = ['Numéro', 'Département'];  


  constructor(private requestService : RequestService) {}

  public filter(): void {
    const filterValue = this.input.nativeElement.value.toLowerCase();
    this.filteredOptions = this.options.filter(o => o.nom.toLowerCase().includes(filterValue));
  }

  public optionSelected(){
    if(this.region_FC.value)
    this.requestService.getDepartementByCodeRegion(this.region_FC.value.toString()).subscribe(datas => {
      this.departements = datas
    })
  }

  viewDepartement(departement : Departement){
    this.viewDepartementEvent.emit(departement)
  }
}
