import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FranceRegionComponent } from './pages/france-region/france-region.component';
import { FranceVilleComponent } from './pages/france-ville/france-ville.component';

const routes: Routes = [
  {path : '', component : FranceRegionComponent},
  {path : 'ville/:idDepartement', component : FranceVilleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeographieRoutingModule { }
