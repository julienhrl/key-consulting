import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IconAddComponent } from './components/icon-add/icon-add.component';



@NgModule({
  declarations: [
    IconAddComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    IconAddComponent
  ]
})
export class IconsModule { }
