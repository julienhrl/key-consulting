export interface VilleI {
    nom : string,
    code : number,
    codeDepartement : number,
    siren : number,
    codeEpci : number,
    codeRegion : number,
    codesPostaux : number[],
    population : number

}
