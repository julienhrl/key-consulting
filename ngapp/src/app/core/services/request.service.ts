import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Region } from '../models/region';
import { Departement } from '../models/departement';
import { Ville } from '../models/ville';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private http : HttpClient) { }

  public getAllRegion():Observable<Region[]>{
    return this.http.get<Region[]>('https://geo.api.gouv.fr/regions')
  }

  public getDepartementByCodeRegion(code : string){
    return this.http.get<Departement[]>(`https://geo.api.gouv.fr/regions/${code}/departements`)
  }

  public getVillesByCodeDepartement(code : string){
    return this.http.get<Ville[]>(`https://geo.api.gouv.fr/departements/${code}/communes`)
  }

  public getDepartementByCode(code : string){
    return this.http.get<Departement[]>(`https://geo.api.gouv.fr/departements?code=${code}`)
  }
}
