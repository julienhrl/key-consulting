import { VilleI } from "../interfaces/ville-i";

export class Ville implements VilleI{
    nom !: string;
    code !: number;
    codeDepartement !: number;
    siren !: number;
    codeEpci !: number;
    codeRegion !: number;
    codesPostaux !: number[];
    population !: number;

    constructor(obj?:Partial<Ville>){
        if(obj){Object.assign(this,obj)}
    }
}
