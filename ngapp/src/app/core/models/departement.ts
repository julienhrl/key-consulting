import { DepartementI } from "../interfaces/departement-i";

export class Departement implements DepartementI{
    nom !: string;
    code !: number;
    codeRegion !: number;

    constructor(obj?:Partial<Departement>){
        if(obj){Object.assign(this,obj)}
    }
}
