import { RegionI } from "../interfaces/region-i";

export class Region implements RegionI{
    nom !: string;
    code !: number;
    score !: number;

    constructor(obj?:Partial<Region>){
        if(obj){Object.assign(this,obj)}
    }
}
